/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author Miki
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private TextField textInputField;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Hello World!");
        //label.setText(textInputField.getText());
        //textInputField.clear();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        label.setText("Default text");
    }    

    //real-time input method
    // [ACTIVE at this BUILD]
    @FXML
    private void textFieldAction2(KeyEvent event) {
        label.textProperty().bind(textInputField.textProperty());
    }
    
    
    //press enter -method
    //[UNACTIVE at this BUILD]
    /*
    @FXML 
    private void textFieldAction(ActionEvent event) {
        label.setText(textInputField.getText());
    }
    */      
    
}
