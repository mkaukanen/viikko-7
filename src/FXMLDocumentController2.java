

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Miki
 */
public class FXMLDocumentController2 implements Initializable {
    
    @FXML
    private TextArea TextAreaField;
    @FXML
    private Button loadButton;
    @FXML
    private Button SaveButton;
    @FXML
    private Button clearButton;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void LoadAction(ActionEvent event){
        Scanner scan = new Scanner(System.in);
        String line;
        System.out.print("Anna tiedoston nimi: ");
        String filename = scan.next();
        BufferedReader in;
        try {
            in = new BufferedReader(new FileReader(filename));
            StringBuilder filetext = new StringBuilder();
            line = in.readLine();
            while (line != null){
                filetext.append(line);
                filetext.append("\n");
                line=in.readLine();
        }
        String areatext = filetext.toString();
        TextAreaField.setText(areatext);
        System.out.println("Lataus onnistui!");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FXMLDocumentController2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController2.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }


    @FXML
    private void SaveAction(ActionEvent event){
        Scanner scan = new Scanner(System.in);
        System.out.print("Anna tiedoston nimi: ");
        String filename = scan.next();
        BufferedWriter out;
        try {
            out = new BufferedWriter(new FileWriter(filename));
            out.write(TextAreaField.getText());
            out.close();
            System.out.println("Tallennus onnistui!");
        
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController2.class.getName()).log(Level.SEVERE, null, ex);
        }  

    }

    @FXML
    private void cleartextAction(ActionEvent event) {
        TextAreaField.setText("");
    }



}
